package contarini.com.desafio_bradesco.data.model

import java.io.Serializable

data class UserResponse(var login : String = "",
                        var avatar_url : String = "") : Serializable