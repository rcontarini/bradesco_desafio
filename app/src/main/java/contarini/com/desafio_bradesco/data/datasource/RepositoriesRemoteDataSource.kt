package contarini.com.desafio_bradesco.data.datasource

import contarini.com.desafio_bradesco.NetworkConstants
import contarini.com.desafio_bradesco.data.ServiceGenerator
import contarini.com.desafio_bradesco.data.interceptors.UnauthorisedInterceptor
import contarini.com.desafio_bradesco.data.service.RepositoryService


object RepositoriesRemoteDataSource {

    private var mService = ServiceGenerator.createService(
        interceptors = listOf(UnauthorisedInterceptor()),
        serviceClass = RepositoryService::class.java,
        url = NetworkConstants.SEARCH_REPOSITORIES
    )

    fun getRepositories(language : String, sort : String, page : Int) = mService.getIncidents(language, sort, page)

}