package contarini.com.desafio_bradesco.data.repository

import contarini.com.desafio_bradesco.data.datasource.RepositoriesRemoteDataSource

object RepositoriesRepository {

    fun getRepositories(language : String, sort : String, page : Int) = RepositoriesRemoteDataSource.getRepositories(language, sort, page)
}