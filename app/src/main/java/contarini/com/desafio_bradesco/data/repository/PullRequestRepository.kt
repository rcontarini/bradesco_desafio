package contarini.com.desafio_bradesco.data.repository

import contarini.com.desafio_bradesco.data.datasource.PullRequestRemoteDataSource

object PullRequestRepository {

    fun getPullRequests(creator : String, repository : String) = PullRequestRemoteDataSource.getPullRequests(creator, repository)

}