package contarini.com.desafio_bradesco.data.model

import java.io.Serializable

data class BaseRepositoriesResponse(var total_count: Int,
                                    var items : ArrayList<RepositoriesResponse>) : Serializable