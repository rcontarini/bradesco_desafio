package contarini.com.desafio_bradesco.ui.request

import contarini.com.desafio_bradesco.data.repository.PullRequestRepository
import contarini.com.desafio_bradesco.extensions.singleSubscribe
import io.reactivex.disposables.CompositeDisposable

class PullRequestModel(private val mPresenter : PullRequestContract.Presenter) : PullRequestContract.Model {

    private var mDisposable = CompositeDisposable()

    override fun loadPullRequest(creator: String, repository: String) {
        mDisposable.add(
            PullRequestRepository.getPullRequests(creator, repository).singleSubscribe(
                onSuccess = {
                    mPresenter.setPullRequests(it)

                },
                onError = {
                    mPresenter.setError(it)
                }
            ))
    }

}