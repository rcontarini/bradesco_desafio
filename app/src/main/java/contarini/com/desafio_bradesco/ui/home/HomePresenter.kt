package contarini.com.desafio_bradesco.ui.home

import contarini.com.desafio_bradesco.data.model.BaseRepositoriesResponse
import contarini.com.desafio_bradesco.data.model.RepositoriesResponse


class HomePresenter : HomeContract.Presenter {

    private var mView: HomeContract.View? = null
    private var model: HomeContract.Model = HomeModel(this)

    override fun detachView() {
        mView = null
    }

    override fun attachView(mvpView: HomeContract.View?) {
        mView = mvpView
    }

    override fun getRepositories(page: Int) {
        mView?.showLoading(true)
        model.loadRepositories(page)
    }

    override fun onClickItem(item: RepositoriesResponse) {
        mView?.loadRepositorieDetail(item)
    }

    override fun setRepositories(repositories: BaseRepositoriesResponse) {
        mView?.displayRepositories(repositories)
        mView?.showLoading(false)
    }

    override fun setError(error: Throwable) {
        mView?.showLoading(false)
        mView?.displayError(error.message)
    }

}