package contarini.com.desafio_bradesco.ui.home

import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import contarini.com.desafio_bradesco.R
import contarini.com.desafio_bradesco.extensions.setup
import contarini.com.desafio_bradesco.data.model.BaseRepositoriesResponse
import contarini.com.desafio_bradesco.data.model.PullRequestResponse
import contarini.com.desafio_bradesco.data.model.RepositoriesResponse
import contarini.com.desafio_bradesco.extensions.startActivitySlideTransition
import contarini.com.desafio_bradesco.ui.base.BaseActivity
import contarini.com.desafio_bradesco.ui.request.createPullRequestIntent
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : BaseActivity(), HomeContract.View {

    private var pageCount : Int = 1
    lateinit var layoutManager : LinearLayoutManager
    private var doubleBackToExitPressedOnce : Boolean = false

    private val mPresenter : HomeContract.Presenter by lazy {
        val presenter = HomePresenter()
        presenter.attachView(this)
        presenter
    }

    private val mAdapter by lazy {
        val adapter = HomeAdapter(context, object : HomeAdapter.OnItemClickListener{
            override fun onItemClicked(item: RepositoriesResponse) {
                mPresenter.onClickItem(item)
            }
        })
        layoutManager = LinearLayoutManager(context)
        rvHome.setup(adapter, layoutManager = layoutManager)
        adapter
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        setup()
        setListeners()
        mPresenter.getRepositories(pageCount)
    }

    private fun setListeners() {
        rvHome.addOnScrollListener( object  : RecyclerView.OnScrollListener(){
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val childCount = layoutManager.childCount
                val visibleItemPosition = layoutManager.findFirstCompletelyVisibleItemPosition()

                if(progressBar.visibility == View.GONE){
                    if(visibleItemPosition + childCount == mAdapter.list.size){
                        pageCount++
                        mPresenter.getRepositories(pageCount)
                    }
                }
                super.onScrolled(recyclerView, dx, dy)
            }
        })

    }

    private fun setup(){
        setToolbar(getString(R.string.title_activity_home), false)
    }

    override fun displayError(msg: String?) {
        Snackbar.make(contentHome, msg!!, Snackbar.LENGTH_LONG)
            .setAction(getString(R.string.error)) { mPresenter.getRepositories(pageCount) }
            .show()
    }

    override fun displayRepositories(repositories: BaseRepositoriesResponse) {
        mAdapter.setRepositories(repositories.items)
        mAdapter.notifyDataSetChanged()
        showList()
    }

    private fun showList(){
        rvHome.visibility = View.VISIBLE
    }

    override fun loadRepositorieDetail(item: RepositoriesResponse) {
        startActivitySlideTransition(createPullRequestIntent(item))
    }

    override fun showLoading(loading: Boolean) {
        if(loading){
            progressBar.visibility = View.VISIBLE
        } else {
            progressBar.visibility = View.GONE
        }
    }

    override fun onBackPressed() {
        if(doubleBackToExitPressedOnce){
            super.finish()
        }
        doubleBackToExitPressedOnce = true
        Toast.makeText(this, getString(R.string.exit), Toast.LENGTH_SHORT).show()

        Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)
    }

}
