package contarini.com.desafio_bradesco.ui.home

import contarini.com.desafio_bradesco.data.model.BaseRepositoriesResponse
import contarini.com.desafio_bradesco.data.model.RepositoriesResponse
import contarini.com.desafio_bradesco.ui.base.BasePresenter
import contarini.com.desafio_bradesco.ui.base.BaseView


interface HomeContract {

    interface View : BaseView<Presenter> {
        fun displayError(msg: String?)
        fun displayRepositories(repositories : BaseRepositoriesResponse)
        fun loadRepositorieDetail(item : RepositoriesResponse)
        fun showLoading(loading : Boolean)
    }

    interface Presenter : BasePresenter<View> {
        fun getRepositories(page : Int)
        fun onClickItem(item : RepositoriesResponse)
        fun setRepositories(repositories : BaseRepositoriesResponse)
        fun setError(error : Throwable)
    }

    interface Model {
        fun loadRepositories(page : Int)
    }
}