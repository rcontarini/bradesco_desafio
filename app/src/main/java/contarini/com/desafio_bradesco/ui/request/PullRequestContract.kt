package contarini.com.desafio_bradesco.ui.request

import contarini.com.desafio_bradesco.data.model.PullRequestResponse
import contarini.com.desafio_bradesco.ui.base.BasePresenter
import contarini.com.desafio_bradesco.ui.base.BaseView


interface PullRequestContract {

    interface View : BaseView<Presenter> {
        fun displayError(msg: String?)
        fun displayPullRequests(requests : List<PullRequestResponse>)
        fun showLoading(loading : Boolean)
        fun startWebRequest(requests: PullRequestResponse)
    }

    interface Presenter : BasePresenter<View> {
        fun getPullRequest(creator : String, repository : String)
        fun setPullRequests(requests : List<PullRequestResponse>)
        fun setError(error : Throwable)
        fun onItemClick(request : PullRequestResponse)
    }

    interface Model {
        fun loadPullRequest(creator : String, repository : String)
    }
}