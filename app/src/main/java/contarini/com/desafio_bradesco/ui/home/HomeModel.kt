package contarini.com.desafio_bradesco.ui.home

import contarini.com.desafio_bradesco.Constants
import contarini.com.desafio_bradesco.extensions.singleSubscribe
import contarini.com.desafio_bradesco.data.repository.RepositoriesRepository
import io.reactivex.disposables.CompositeDisposable


class HomeModel(private val mPresenter : HomeContract.Presenter) : HomeContract.Model {

    private var mDisposable = CompositeDisposable()

    override fun loadRepositories(page: Int) {
        mDisposable.add(
            RepositoriesRepository.getRepositories(Constants.LANGUAGE_API, Constants.SORT_API, page).singleSubscribe(
                onSuccess = {
                    mPresenter.setRepositories(it)
                },

                onError = {
                    mPresenter.setError(it)
                }
            ))
    }


}